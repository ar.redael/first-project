import React from 'react'
import './HeaderMenu.css'

export default class HeaderMenu extends React.Component {
  render () {
    return (
      <div className="container">
        <img src="https://icon.cat/img/icon_loop.png" alt="Company Logo" className="company-logo" />
        <h1 className="title">{this.props.companyName}</h1>
      </div>
    )
  }
}
