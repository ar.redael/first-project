import React from 'react'
import './AddItemForm.css'
import axios from 'axios'

export default class AddItemForm extends React.Component {
  state = {
    form: {}
  }
  clearInput = () => {
    this.props.onClick()
    document.getElementById('myForm').reset()
  }

  submitForm = event => {
    // document.getElementById('myForm').submit()
    event.preventDefault()
    this.postUser()
  }

  handleFormChange = name => event => {
    const { form } = this.state
    const newValue = { [name]: event.target.value }
    const newForm = { ...form, ...newValue }
    this.setState({ form: newForm })
  }

  postUser = async () => {
    try {
      const response = await axios.post('http://localhost:3000/api/beers', this.state.form)
      console.log('response: ', response)
    } catch (error) {
      console.log('error: ', error)
    }
  }

  fields = () => {
    const field = this.props.formFields.map((item, k) => (
      <div key={`field-${k}`}>
        <label>{item}:</label>
        <br />
        <input type="text" onChange={this.handleFormChange(item.toLowerCase())} />
      </div>
    ))
    return field
  }

  render () {
    return (
      <div className="form-container">
        <form id="myForm">
          {this.fields()}
          <button className="cancel-btn" type="button" onClick={this.clearInput}>
            Cancel
          </button>
          <button className="add-btn" type="submit" value="Cadastrar" onClick={this.submitForm}>
            Add
          </button>
        </form>
      </div>
    )
  }
}
