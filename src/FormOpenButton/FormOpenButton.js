import React from 'react'
import './FormOpenButton.css'
import AddItemForm from '../AddItemForm/AddItemForm'

export default class FormOpenButton extends React.Component {
  state = {
    isClicked: false,
    clicked: '',
    visible: 'visible'
  }

  handleClick = () => {
    this.setState({
      isClicked: !this.state.isClicked,
      visible: this.state.isClicked ? 'visible' : ''
    })
  }

  render () {
    const clicked = this.state.isClicked ? 'color-red' : ''
    return (
      <div>
        <div className={'popup ' + this.state.visible}>
          <div className="form-wrapper">
            <AddItemForm
              formFields={[
                'Title',
                'Description',
                'Price'
              ]}
              onClick={this.handleClick}
            />
          </div>
        </div>
        <button id="button" className={'round-button ' + clicked} onClick={this.handleClick}>
          {this.props.content}
        </button>
      </div>
    )
  }
}
