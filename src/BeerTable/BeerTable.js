import React from 'react'
import axios from 'axios'
import DataTable from '../table/DataTable'

export default class BeerTable extends React.Component {
  state = {
    beerList: [],
    pagination: {
      total: 0,
      page: 1,
      totalPages: 0
    }
  }

  componentDidMount () {
    this.getTotal()
  }

  getTotal = async () => {
    const { data: response } = await axios.get('http://localhost:3000/api/beers')
    const total = response.total
    this.setState({ pagination: { ...this.state.pagination, total } })
    this.loadBeerList({ limit: total })
    return response.total
  }

  async loadBeerList ({ limit: _limit = null, page = 1 } = {}) {
    const limit = _limit ? _limit : this.state.pagination.total
    const uri = 'http://localhost:3000/api/beers?page=' + page + '&limit=' + limit
    const { data: response } = await axios.get(uri).catch(err => {
      console.error('failed to fetch from api:', err.message)
      throw err
    })
    this.setState({
      beerList: response.docs,
      pagination: { ...this.state.pagination, totalPages: Number(response.pages) }
    })
  }

  postBeer = async newBeer => {
    try {
      const response = await axios.post('http://localhost:3000/api/beers', newBeer)
      console.log('response: ', response)
      await this.loadBeerList()
    } catch (error) {
      console.log('error: ', error)
    }
  }

  deleteBeer = async oldData => {
    try {
      const itemIndice = oldData.tableData.id
      const deleteItem = this.state.beerList[itemIndice]._id
      const response = await axios.delete('http://localhost:3000/api/beers/' + deleteItem)
      console.log('responde: ', response)
      await this.loadBeerList()
    } catch (error) {
      console.log('error: ', error)
    }
  }

  render () {
    return (
      <div>
        <DataTable
          save={this.postBeer}
          delete={this.deleteBeer}
          rows={this.state.beerList}
          pagination={this.state.pagination}
          update={this.updatePagination}
        />
      </div>
    )
  }
}
