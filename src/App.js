import React from 'react'
import './App.css'
import HeaderMenu from './HeaderMenu/HeaderMenu'
import BasicLineChart from './chart/BasicLineChart'
import BeerTable from './BeerTable/BeerTable'
import FormOpenButton from './FormOpenButton/FormOpenButton'

function App () {
  return (
    <div className="App">
      <header>
        <HeaderMenu companyName="Test Project" />
      </header>
      <div className="chart-wrapper">
        <BasicLineChart
          series={[
            {
              name: 'Desktops',
              data: [
                10,
                41,
                35,
                51,
                49,
                62,
                69,
                91,
                148
              ]
            }
          ]}
          title="TEST PROJECT"
        />
      </div>
      <div className="table-wrapper">
        <BeerTable />
      </div>
    </div>
  )
}

export default App
