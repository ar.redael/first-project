import React from 'react'
import './BasicLineChart.css'
import ReactApexChart from 'react-apexcharts'

class BasicLineChart extends React.Component {
  state = {
    options: {
      chart: {
        background: '#fff',
        zoom: {
          enabled: false
        },
        foreColor: 'darkslategray'
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'straight'
      },
      title: {
        text: this.props.title,
        align: 'left'
      },
      grid: {
        row: {
          colors: [
            '#f3f3f3',
            'transparent'
          ], // takes an array which will be repeated on columns
          opacity: 0.5
        }
      },
      xaxis: {
        style: {
          fontSize: '12px',
          fontFamily: 'Helvetica, Arial, sans-serif',
          cssClass: 'apexcharts-xaxis-label'
        },
        categories: [
          'Jan',
          'Feb',
          'Mar',
          'Apr',
          'May',
          'Jun',
          'Jul',
          'Aug',
          'Sep'
        ]
      }
    }
  }
  render () {
    const chart = <ReactApexChart options={this.state.options} series={this.props.series} type="line" height="350" />
    return <div id="chart">{chart}</div>
  }
}

export default BasicLineChart
