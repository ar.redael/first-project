import React from 'react'
import MaterialTable from 'material-table'

export default class DataTable extends React.Component {
  state = {
    columns: [
      { title: 'Title', field: 'title' },
      { title: 'Description', field: 'description' },
      {
        title: 'Price',
        field: 'price'
      }
    ],
    data: [],
    options: {
      actionsColumnIndex: -1,
      exportButton: true,
      pageSize: 10,
      pageSizeOptions: [
        10,
        20,
        50
      ]
    }
  }

  render () {
    const rows = (this.props.rows || []).map(({ title, description, price }) => ({
      title,
      description,
      price: price.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })
    }))
    const data = [
      ...this.state.data,
      ...rows
    ]
    return (
      <MaterialTable
        title="Editable Example"
        columns={this.state.columns}
        data={data}
        options={this.state.options}
        editable={{
          onRowAdd: this.props.save || (v => null),
          onRowDelete: this.props.delete || (v => null)
        }}
      />
    )
  }
}
